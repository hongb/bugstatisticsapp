package com.lufax.bugSearch.service.Impl;

import com.lufax.bugSearch.domain.Bug;
import com.lufax.bugSearch.domain.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.testng.Assert.*;

/**
 * Created by hongbo010 on 2019/12/19.
 */
public class BugServiceImplTest {
    private Group jvFirstGroup = new Group(1);
    ArrayList<String> jvFirstUmidList = new ArrayList();


    public void setJvFirstUmidList(ArrayList<String> jvFirstUmidList) {
        this.jvFirstUmidList = jvFirstUmidList;
    }

    public ArrayList<String> getJvFirstUmidList() {
        return jvFirstUmidList;
    }


    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    BugServiceImpl bugServiceImpl = (BugServiceImpl) context.getBean("bugServiceImpl");

    @Test
    public void testBugSearch(String umid) throws Exception {
        List<Bug> list = bugServiceImpl.bugSearch(umid);
        System.out.println(list);
    }

    @Test
    public void testBugSearch(String umid, String severity) throws Exception {
        List<Bug> list = bugServiceImpl.bugSearch(umid, severity);
        System.out.println(list);
    }

    @Test
    public void testBugSearch(Group group) throws Exception {
        List<Bug> list = bugServiceImpl.bugSearch(group);
        System.out.println(list);
    }

    @Test
    public void testBugSearch(Group group, String severity) throws Exception {
        List<Bug> list = bugServiceImpl.bugSearch(group,severity);
        System.out.println(list);
    }

    @Test
    public void testBugSearch(String umid, String startime, String endTime) throws Exception {
        List<Bug> list = bugServiceImpl.bugSearch(umid,startime, endTime);
        System.out.println(list);
    }

    @Test
    public void testBugCount(String umid) throws Exception {

    }

    @Test
    public void testBugSeverityCount(String umid) throws Exception {
    }

    @Test
    public void testGroupBugSeverityCount(Group group) throws Exception {
    }

    @Test
    public void testBugCount(String umid, String startime, String endTime) throws Exception {
    }

    @Test
    public void testBugCount(Group group, String startime, String endTime) throws Exception {
    }

    @Test
    public void testGroupBugSeverityCount(Group group, String startime, String endTime) throws Exception {
    }

}