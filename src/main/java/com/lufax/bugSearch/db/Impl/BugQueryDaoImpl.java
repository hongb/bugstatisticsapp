package com.lufax.bugSearch.db.Impl;

import com.lufax.bugSearch.db.BugQueryDao;
import com.lufax.bugSearch.domain.Bug;
import com.lufax.bugSearch.domain.Group;
import com.lufax.bugSearch.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by hongbo010 on 2019/12/16.
 */
@Repository
public class BugQueryDaoImpl implements BugQueryDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    private String BASE_SQL_COLUMN = "t2.real_name,t1.id,summary,tester_owner,dev_owner,severity,resolution,t1.created_at,reject_count ";
    private static final String BUG_TABLE_NAME = " implement_bug_entry ";
    private static final String USER_TABLE_NAME = " irelease2_cost_record_user_department_map ";
    private static final String IS_VALID_BUG = " and resolution!= '无效bug' ";
    private static final String COUNT_BASE_SQL_COLUMN = " t2.real_name, t1.dev_owner ";

    @Override
    /*
    * 根据开发umid号查询bug记录
    * @return bug类型的列表
    * department_id 陆金所国际JV深圳研发团队 9770
    */
    public List<Bug> queryBug(String umid) {
        String sql = "SElECT " + BASE_SQL_COLUMN + " FROM " + BUG_TABLE_NAME + " t1, " + USER_TABLE_NAME + " t2 " +
                " WHERE t1.dev_owner = t2.who and t1.dev_owner=?" + IS_VALID_BUG + "and t2.is_valid=1 and t2.department_id='9770'";
        List<Bug> list = jdbcTemplate.query(sql, new BugQueryMapper(), umid);
        return list;
    }

    public List<Bug> queryBug(String umid, String startTime, String endTime ){
        String sql = "SElECT " + BASE_SQL_COLUMN + " FROM " + BUG_TABLE_NAME + " t1, " + USER_TABLE_NAME + " t2 " +
                " WHERE t1.dev_owner = t2.who and t1.dev_owner=?" + IS_VALID_BUG  +" and t1.created_at BETWEEN ? and ? and t2.is_valid=1 and t2.department_id='9770'";
        List<Bug> list = jdbcTemplate.query(sql, new BugQueryMapper(), umid, startTime, endTime);
        return list;
    }
    @Override
    public List<Bug> queryBug(String umid, String severity) {
        String sql = "SElECT " + BASE_SQL_COLUMN + " FROM " + BUG_TABLE_NAME + " t1, " + USER_TABLE_NAME + " t2 " +
                " WHERE t1.dev_owner = t2.who and t1.dev_owner=? " + IS_VALID_BUG + "and t2.is_valid=1 and t2.department_id='9770' and t1.severity=? ";
        List<Bug> list = jdbcTemplate.query(sql, new BugQueryMapper(), umid, severity);
        return list;
    }

    @Override
    public List<Bug> queryBug(String umid, String severity, String startTime, String endTime) {
        String sql = "SElECT " + BASE_SQL_COLUMN + " FROM " + BUG_TABLE_NAME + " t1, " + USER_TABLE_NAME + " t2 " +
                " WHERE t1.dev_owner = t2.who and t1.dev_owner=?" + IS_VALID_BUG  + " and t1.severity=? and t1.created_at BETWEEN ? and ? and t2.is_valid=1 and t2.department_id='9770' ";
        List<Bug> list = jdbcTemplate.query(sql, new BugQueryMapper(), umid, severity, startTime, endTime);
        return list;
    }

    @Override
    public List<Bug> queryBug(Group group) {
        String sql = "SElECT " + BASE_SQL_COLUMN + " FROM " + BUG_TABLE_NAME + " t1, " + USER_TABLE_NAME + " t2 " +
                " WHERE t1.dev_owner = t2.who and t1.dev_owner in  " + group.getGroupUmid() + IS_VALID_BUG + " and t2.is_valid=1 and t2.department_id='9770'";
        List<Bug> list = jdbcTemplate.query(sql, new BugQueryMapper());
        return list;
    }

    @Override
    public List<Bug> queryBug(Group group, String startTime, String endTime) {
        String sql = "SElECT " + BASE_SQL_COLUMN + " FROM " + BUG_TABLE_NAME + " t1, " + USER_TABLE_NAME + " t2 " +
                " WHERE t1.dev_owner = t2.who and t1.dev_owner in  " + group.getGroupUmid() + IS_VALID_BUG + " and t1.created_at BETWEEN ? and ? and t2.is_valid=1 and t2.department_id='9770'";
        List<Bug> list = jdbcTemplate.query(sql, new BugQueryMapper(),startTime, endTime);
        return list;
    }

    @Override
    public List<Bug> queryBug(Group group, String severity, String startTime, String endTime){
        String sql = "SElECT " + BASE_SQL_COLUMN + " FROM " + BUG_TABLE_NAME + " t1, " + USER_TABLE_NAME + " t2 " +
                " WHERE t1.dev_owner = t2.who and t1.dev_owner in  " + group.getGroupUmid() + IS_VALID_BUG + " and t1.severity=? and t1.created_at BETWEEN ? and ? and t2.is_valid=1 and t2.department_id='9770'";
        List<Bug> list = jdbcTemplate.query(sql, new BugQueryMapper(),severity, startTime, endTime);
        return list;
    }


    @Override
    public List<Bug> queryBug(Group group, String severity) {
        String sql = "SElECT " + BASE_SQL_COLUMN + " FROM " + BUG_TABLE_NAME + " t1, " + USER_TABLE_NAME + " t2 " +
                " WHERE t1.dev_owner = t2.who and t1.dev_owner in  " + group.getGroupUmid() + IS_VALID_BUG + " and t2.is_valid=1 and t1.severity = ? and t2.department_id='9770'";
        List<Bug> list = jdbcTemplate.query(sql, new BugQueryMapper(), severity);
        return list;
    }

    /*
    * 根据开发umid号查询bug数量
    */
    @Override
    public int queryCountBug(String umid) {
        String sql = "SELECT count(*) from " + BUG_TABLE_NAME + "WHERE dev_owner=? " + IS_VALID_BUG;
        try {
            int count = jdbcTemplate.queryForObject(sql, Integer.class, umid);
            return count;
        } catch (DataAccessException e) {
            return 0;
        }
    }

    public int queryCountBugByVersion(String umid) {
        String sql = "SELECT count(*) from " + BUG_TABLE_NAME + "WHERE dev_owner=? and created_at > '2019-10-09'" + IS_VALID_BUG;
        try {
            int count = jdbcTemplate.queryForObject(sql, Integer.class, umid);
            return count;
        } catch (DataAccessException e) {
            return 0;
        }
    }


    /*
    * 根据开发umid号查询bug数量
    * @param severity  致命，严重，一般，小错误
    * @return int
    */
    @Override
    public int queryCountBug(String umid, String severity) {
        String sql = "SELECT count(*) from " + BUG_TABLE_NAME + "WHERE dev_owner=? and severity=? " + IS_VALID_BUG + "GROUP BY dev_owner";
        try {
           int count = jdbcTemplate.queryForObject(sql, Integer.class, umid, severity);
           //重复查库，加重查询负载
           //int count = this.queryBug(umid, severity).size();
            return count;
        } catch (DataAccessException e) {
            return 0;
        }
    }

    @Override
    public int queryCountBug(String umid, String startTime, String endTime) {
        String sql = "SELECT count(*) from " + BUG_TABLE_NAME + "WHERE  dev_owner=? " + IS_VALID_BUG + " and created_at BETWEEN ? and ? " + " GROUP BY dev_owner";
        try {
            int count = jdbcTemplate.queryForObject(sql, Integer.class, umid, startTime, endTime);
            return count;
        } catch (DataAccessException e) {
            return 0;
        }
    }

    public int queryCountSeverityBugByVersion(String umid, String severity) {
        String sql = "SELECT count(*) from " + BUG_TABLE_NAME + "WHERE  dev_owner=? " + IS_VALID_BUG + " and created_at > '2019-10-09' and severity = ? " + " GROUP BY dev_owner";
        try {
            int count = jdbcTemplate.queryForObject(sql, Integer.class, umid, severity);
            return count;
        } catch (DataAccessException e) {
            return 0;
        }
    }


    @Override
    public int queryCountBug(String umid, String severity, String startTime, String endTime) {
        String sql = "SELECT count(*) from " + BUG_TABLE_NAME +
                "WHERE  dev_owner=? " + IS_VALID_BUG + " and severity =? and created_at BETWEEN ? and ? " + " GROUP BY dev_owner";
        try {
            int count = jdbcTemplate.queryForObject(sql, Integer.class, umid, severity, startTime, endTime);
            return count;
        } catch (DataAccessException e) {
            return 0;
        }
    }


    class BugQueryMapper implements RowMapper<Bug> {
        @Override
        public Bug mapRow(ResultSet rs, int rowNum) throws SQLException {
            Bug bug = new Bug();
            User user = new User(rs.getString("dev_owner"));
            user.setUserName(rs.getString("real_name"));
            bug.setUser(user);
            bug.setBugId(rs.getInt("id"));
            bug.setTester_owner(rs.getString("tester_owner"));
            bug.setSunmmary(rs.getString("summary"));
            bug.setDev_owner(rs.getString("dev_owner"));
            bug.setCreated_at(rs.getDate("created_at"));
            bug.setReject_count(rs.getInt("reject_count"));
            bug.setResolution(rs.getString("resolution"));
            bug.setSeverity(rs.getString("severity"));
            //user.setUserName(rs.getString("real_name"));
            return bug;
        }
    }

}
