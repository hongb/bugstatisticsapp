package com.lufax.bugSearch.db;

import com.lufax.bugSearch.domain.User;

import java.util.List;

/**
 * Created by hongbo010 on 2019/12/16.
 */
public interface UserDao {
    /*
    * 根据umid查询对应的姓名
    * */

    List<User> queryName(String umid);
}
