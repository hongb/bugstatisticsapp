package com.lufax.bugSearch.db;

import com.lufax.bugSearch.domain.Bug;
import com.lufax.bugSearch.domain.Group;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

/**
 * Created by hongbo010 on 2019/12/16.
 */

@Repository
public interface BugQueryDao {

    List<Bug> queryBug(String umid);

    List<Bug> queryBug(String umid, String severity);

    List<Bug> queryBug(String umid, String severity, String startTime, String endTime);

    List<Bug> queryBug(Group group);

    List<Bug> queryBug(Group group, String startTime, String endTime);

    List<Bug> queryBug(Group group, String severity);

    List<Bug> queryBug(Group group, String severity, String startTime, String endTime);

    int queryCountBug(String umid);

    int queryCountBug(String umid, String severity);

    int queryCountBug(String umid, String startTime, String endTime);

    int queryCountBug(String umid, String severity, String startTime, String endTime);



}
