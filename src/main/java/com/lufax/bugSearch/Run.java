package com.lufax.bugSearch;

import com.lufax.bugSearch.domain.Group;
import com.lufax.bugSearch.service.Impl.BugServiceImpl;
import com.lufax.bugSearch.util.WriteExcle;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by hongbo010 on 2019/12/19.
 */

public class Run {


    public static void main(String args[]) {

        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        BugServiceImpl bugServiceImpl = (BugServiceImpl)context.getBean("bugServiceImpl");
        Group jvFristGroup = Group.getJvFristGroup();
       List<List<Integer>> list = bugServiceImpl.groupBugSeverityCount(jvFristGroup,"2019-12-01","2019-12-22");
        //List<Bug>list1 = bugServiceImpl.bugSearch("chenchunyong083","2019-12-01","2019-12-22");
        System.out.println(list);
     //   WriteExcle.writeExcel(list,10, "/Users/hongbo010/Documents/abc.xlsx");

    }
}