package com.lufax.bugSearch.service.Impl;

import com.lufax.bugSearch.db.Impl.BugQueryDaoImpl;
import com.lufax.bugSearch.domain.Bug;
import com.lufax.bugSearch.domain.Group;
import com.lufax.bugSearch.domain.User;
import com.lufax.bugSearch.service.BugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hongbo010 on 2019/12/16.
 */

@Service
public class BugServiceImpl implements BugService {

    private Bug bug;

    private User user;

    private Group group;

    @Autowired
    private BugQueryDaoImpl bugQueryDaoImpl;

    @Override
    public List<Bug> bugSearch(String umid) {
        List<Bug> bugLists = bugQueryDaoImpl.queryBug(umid);
        return bugLists;
    }

    @Override
    public List<Bug> bugSearch(String umid, String severity) {
        List<Bug> bugLists = bugQueryDaoImpl.queryBug(umid, severity);
        return bugLists;
    }

    @Override
    public List<Bug> bugSearch(Group group) {
        List<Bug> bugLists = bugQueryDaoImpl.queryBug(group);
        return bugLists;
    }

    @Override
    public List<Bug> bugSearch(Group group, String severity) {
        List<Bug> bugLists = bugQueryDaoImpl.queryBug(group, severity);
        return bugLists;
    }

    public List<Integer> bugSeveritySearch(String umid, String startTime, String endTime) {
        int deadlyBug = bugQueryDaoImpl.queryCountBug(umid, "致命", startTime, endTime);
        int seriousBug = bugQueryDaoImpl.queryCountBug(umid, "严重", startTime, endTime);
        int normalBug = bugQueryDaoImpl.queryCountBug(umid, "一般", startTime, endTime);
        int smallBug = bugQueryDaoImpl.queryCountBug(umid, "小错误", startTime, endTime);
        int sumBug = deadlyBug + seriousBug + normalBug + smallBug;
        List<Integer> severityBug = new ArrayList<>();
        severityBug.add(0, deadlyBug);
        severityBug.add(1, seriousBug);
        severityBug.add(2, normalBug);
        severityBug.add(3, smallBug);
        severityBug.add(4, sumBug);
        return severityBug;
    }

    @Override
    public List bugSearch(String umid, String startime, String endTime) {
        return bugQueryDaoImpl.queryBug(umid, startime, endTime);

    }

    @Override
    public int bugCount(String umid) {
        int count = bugQueryDaoImpl.queryCountBug(umid);
        return count;
    }

    public HashMap<String, Integer> bugCount(Group group) {
        HashMap hashMap = new HashMap();
        for (String umid : group.getGroupListUmid()) {
            int count = bugQueryDaoImpl.queryCountBugByVersion(umid);
            hashMap.put(umid, count);
        }
        return hashMap;
    }

    @Override
    public List<Integer> bugSeverityCount(String umid) {
        int deadlyBug = bugQueryDaoImpl.queryCountBug(umid, "致命");
        int seriousBug = bugQueryDaoImpl.queryCountBug(umid, "严重");
        int normalBug = bugQueryDaoImpl.queryCountBug(umid, "一般");
        int smallBug = bugQueryDaoImpl.queryCountBug(umid, "小错误");
        List<Integer> severityBug = new ArrayList<>();
        severityBug.add(0, deadlyBug);
        severityBug.add(1, seriousBug);
        severityBug.add(2, normalBug);
        severityBug.add(3, smallBug);
        return severityBug;
    }

    @Override
    public HashMap<String, List<Integer>> groupBugSeverityCount(Group group) {
        List<String> umidList = group.getGroupListUmid();
        List<List<Integer>> groupSeverityBugList = new ArrayList<>();
        HashMap groupBugSeverityHashMap = new HashMap();
        for (String umid : umidList) {
            List<Integer> result = this.bugSeverityCount(umid);
            groupSeverityBugList.add(result);
            groupBugSeverityHashMap.put(umid, result);
        }
        return groupBugSeverityHashMap;
    }

    /*统计所有版本组内成员bug数量
    * @param Group
    * retrun HashMap， umid:[每个版本的bug数量]
    * */
    public HashMap bugCountByVersion(Group group) {
        List<String> umidList = group.getGroupListUmid();
        HashMap allVersionGroupSeverityBugMap = new HashMap();
        for (String umid : umidList) {
            List groupSeverityBugList = this.iterTimeBugCount(umid);
            allVersionGroupSeverityBugMap.put(umid, groupSeverityBugList);
        }
        return allVersionGroupSeverityBugMap;
    }

    @Override
    public int bugCount(String umid, String startime, String endTime) {
        int count = bugQueryDaoImpl.queryCountBug(umid, startime, endTime);
        return count;
    }

    @Override
    public HashMap bugCount(Group group, String startime, String endTime) {
        List<String> umidList = group.getGroupListUmid();
        List<Integer> groupBugCountList = new ArrayList<>();
        HashMap groupBugCountMap = new HashMap();
        for (String umid : umidList) {
            int result = this.bugCount(umid, startime, endTime);
            groupBugCountList.add(result);
            groupBugCountMap.put(umid, result);
        }
        return groupBugCountMap;
    }

    @Override
    public List<List<Integer>> groupBugSeverityCount(Group group, String startime, String endTime) {
        List<String> umidList = group.getGroupListUmid();
        List<List<Integer>> groupSeverityBugList = new ArrayList<>();
        for (String umid : umidList) {
            int deadlyBug = bugQueryDaoImpl.queryCountBug(umid, "致命", startime, endTime);
            int seriousBug = bugQueryDaoImpl.queryCountBug(umid, "严重", startime, endTime);
            int normalBug = bugQueryDaoImpl.queryCountBug(umid, "一般", startime, endTime);
            int smallBug = bugQueryDaoImpl.queryCountBug(umid, "小错误", startime, endTime);
            List<Integer> severityBug = new ArrayList<>();
            severityBug.add(0, deadlyBug);
            severityBug.add(1, seriousBug);
            severityBug.add(2, normalBug);
            severityBug.add(3, smallBug);
            groupSeverityBugList.add(severityBug);
        }
        return groupSeverityBugList;
    }

    /**
     * 所有版本bug严重bug总数统计
     */
    public HashMap severityBugCountByVersion(Group group) {
        List<String> umidList = group.getGroupListUmid();
        HashMap groupBugSeverityHashMap = new HashMap();
        for (String umid : umidList) {
            List<List<Integer>> bugSeveritySearchList = new ArrayList();
            bugSeveritySearchList.add(this.bugSeveritySearch(umid, "2019-10-09", "2019-10-23"));
            bugSeveritySearchList.add(this.bugSeveritySearch(umid, "2019-10-23", "2019-11-06"));
            bugSeveritySearchList.add(this.bugSeveritySearch(umid, "2019-11-06", "2019-11-20"));
            bugSeveritySearchList.add(this.bugSeveritySearch(umid, "2019-11-20", "2019-12-04"));
            bugSeveritySearchList.add(this.bugSeveritySearch(umid, "2019-12-04", "2019-12-18"));
            bugSeveritySearchList.add(this.bugSeveritySearch(umid, "2019-12-18", "2020-01-02"));
            groupBugSeverityHashMap.put(umid, bugSeveritySearchList);
        }
        return groupBugSeverityHashMap;
    }

    public HashMap GroupCountSeverityBugByVersion(Group group) {
        List<String> umidList = group.getGroupListUmid();
        HashMap groupBugSeverityHashMap = new HashMap();
        for (String umid : umidList) {
            int deadlyBug = bugQueryDaoImpl.queryCountSeverityBugByVersion(umid, "致命");
            int seriousBug = bugQueryDaoImpl.queryCountSeverityBugByVersion(umid, "严重");
            int normalBug = bugQueryDaoImpl.queryCountSeverityBugByVersion(umid, "一般");
            int smallBug = bugQueryDaoImpl.queryCountSeverityBugByVersion(umid, "小错误");
            int sumBug = deadlyBug + seriousBug + normalBug + smallBug;
            List<Integer> severityBug = new ArrayList<>();
            severityBug.add(0, deadlyBug);
            severityBug.add(1, seriousBug);
            severityBug.add(2, normalBug);
            severityBug.add(3, smallBug);
            severityBug.add(4, sumBug);
            groupBugSeverityHashMap.put(umid, severityBug);
        }
        return groupBugSeverityHashMap;
    }

    /**
     * 目前迭代版本的记录
     *
     * @return list
     */

    public List iterTimeBugCount(String umid) {
        int oneVersionResult = this.oneVersionIterBugCount(umid);
        int twoVersionResult = this.twoVersionIterBugCount(umid);
        int threeVersionResult = this.threeVersionIterBugCount(umid);
        int fourVersionResult = this.fourVersionIterBugCount(umid);
        int fiveVersionResult = this.fiveVersionIterBugCount(umid);
        int sixVersionResult = this.sixVersionIterBugCount(umid);
        int sumResult = oneVersionResult + twoVersionResult + threeVersionResult + fourVersionResult + fiveVersionResult + sixVersionResult;
        List<Integer> groupSeverityBugList = new ArrayList<>();
        groupSeverityBugList.add(oneVersionResult);
        groupSeverityBugList.add(twoVersionResult);
        groupSeverityBugList.add(threeVersionResult);
        groupSeverityBugList.add(fourVersionResult);
        groupSeverityBugList.add(fiveVersionResult);
        groupSeverityBugList.add(sixVersionResult);
        groupSeverityBugList.add(sumResult);
        return groupSeverityBugList;
    }

    /**
     * 1023迭代开发bug统计
     *
     * @param umid return int count
     */
    public int oneVersionIterBugCount(String umid) {
        return this.bugCount(umid, "2019-10-09", "2019-10-23");
    }

    /**
     * 1106迭代开发bug统计
     *
     * @param umid return int count
     */
    public int twoVersionIterBugCount(String umid) {
        return this.bugCount(umid, "2019-10-23", "2019-11-06");
    }

    /**
     * 1120版本迭代开发bug统计
     *
     * @param umid return int count
     */
    public int threeVersionIterBugCount(String umid) {
        return this.bugCount(umid, "2019-11-06", "2019-11-20");
    }

    /**
     * 1204版本迭代开发bug统计
     *
     * @param umid return int count
     */
    public int fourVersionIterBugCount(String umid) {
        return this.bugCount(umid, "2019-11-20", "2019-12-04");
    }

    /**
     * 1218版本迭代开发bug统计
     *
     * @param umid return int count
     */
    public int fiveVersionIterBugCount(String umid) {
        return this.bugCount(umid, "2019-12-04", "2019-12-18");
    }

    /**
     * 0102版本迭代开发bug统计
     *
     * @param umid return int count
     */
    public int sixVersionIterBugCount(String umid) {
        return this.bugCount(umid, "2019-12-18", "2020-01-02");
    }

    /**
     * 1023迭代开发bug统计
     *
     * @param group return int count
     */
    public HashMap oneVersionIterBugCount(Group group) {
        HashMap<String, Integer> hashMap = new HashMap();
        for (String umid : group.getGroupListUmid()) {
            int count = this.oneVersionIterBugCount(umid);
            hashMap.put(umid, count);
        }
        return hashMap;
    }

    /**
     * 1106迭代开发bug统计
     *
     * @param group return int count
     */
    public HashMap twoVersionIterBugCount(Group group) {
        HashMap<String, Integer> hashMap = new HashMap();
        for (String umid : group.getGroupListUmid()) {
            int count = this.twoVersionIterBugCount(umid);
            hashMap.put(umid, count);
        }
        return hashMap;
    }

    /**
     * 1120版本迭代开发bug统计
     *
     * @param group return int count
     */
    public HashMap threeVersionIterBugCount(Group group) {
        HashMap<String, Integer> hashMap = new HashMap();
        for (String umid : group.getGroupListUmid()) {
            int count = this.threeVersionIterBugCount(umid);
            hashMap.put(umid, count);
        }
        return hashMap;
    }

    /**
     * 1204版本迭代开发bug统计
     *
     * @param group return int count
     */
    public HashMap fourVersionIterBugCount(Group group) {
        HashMap<String, Integer> hashMap = new HashMap();
        for (String umid : group.getGroupListUmid()) {
            int count = this.fourVersionIterBugCount(umid);
            hashMap.put(umid, count);
        }
        return hashMap;
    }

    /**
     * 1218版本迭代开发bug统计
     *
     * @param group return int count
     */
    public HashMap fiveVersionIterBugCount(Group group) {
        HashMap<String, Integer> hashMap = new HashMap();
        for (String umid : group.getGroupListUmid()) {
            int count = this.fiveVersionIterBugCount(umid);
            hashMap.put(umid, count);
        }
        return hashMap;
    }

    /**
     * 0102版本迭代开发bug统计
     *
     * @param group return int count
     */
    public HashMap sixVersionIterBugCount(Group group) {
        HashMap<String, Integer> hashMap = new HashMap();
        for (String umid : group.getGroupListUmid()) {
            int count = this.sixVersionIterBugCount(umid);
            hashMap.put(umid, count);
        }
        return hashMap;
    }

}
