package com.lufax.bugSearch.service;

import com.lufax.bugSearch.domain.Bug;
import com.lufax.bugSearch.domain.Group;

import java.util.HashMap;
import java.util.List;

/**
 * Created by hongbo010 on 2019/12/16.
 */

public interface BugService {
    /*
    * 查询单人名下的bug列表
    * */
    List<Bug> bugSearch(String umid);

    List<Bug> bugSearch(String umid, String severity);

    List<Bug> bugSearch(String umid ,String startime, String endTime);

    List<Bug> bugSearch(Group group);

    List<Bug> bugSearch(Group group, String severity);

    int bugCount(String umid);

    int bugCount(String umid, String startime, String endTime);
    /*
    * 查询单人名下的bug总数，p0，p1，p2，p3，bug数量
    * */

    List<Integer>  bugSeverityCount(String umid);

    HashMap<String,List<Integer>> groupBugSeverityCount(Group group);

    HashMap bugCount(Group group, String startime, String endTime);

    List<List<Integer>> groupBugSeverityCount(Group group, String startime, String endTime);

    /**查询组内1023迭代后，bug严重程度的数量
     *
     */
    HashMap GroupCountSeverityBugByVersion(Group group);


}
