package com.lufax.bugSearch.enums;

/**
 * Created by hongbo010 on 2019/12/17.
 */
public enum GroupCodeEnum {

    深圳jv开发一团队(1),

    陆国际团队(2),

    测试团队(3),

    native和H5团队(4),

    jv一后端团队(5),;
    private int code;

    GroupCodeEnum(int code) {

        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static int getGroupCode(int code) {
        for (GroupCodeEnum groupCodeEnum : GroupCodeEnum.values()) {
            if (groupCodeEnum.getCode() == code) {
                return groupCodeEnum.code;
            }
        }
        return 0;
    }
}
