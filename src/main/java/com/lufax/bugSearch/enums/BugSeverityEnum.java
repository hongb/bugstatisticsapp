package com.lufax.bugSearch.enums;

/**
 * Created by hongbo010 on 2019/12/14.
 */
public enum BugSeverityEnum {

    致命("p0"),

    严重("p1"),

    一般("p2"),

    小错误("p3"),
    ;
    BugSeverityEnum (String code){
        this.code =code;
    }

    public String getCode() {
        return code;
    }

    private String code;

    public static BugSeverityEnum getByServerity(String code) {
        for (BugSeverityEnum bugSeverityEnum : BugSeverityEnum.values()) {
            if (bugSeverityEnum.getCode().equals(code)) {
                return bugSeverityEnum;
            }

        }
        return null;
    }


}

