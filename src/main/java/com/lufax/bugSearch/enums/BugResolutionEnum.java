package com.lufax.bugSearch.enums;

/**
 * Created by hongbo010 on 2019/12/14.
 */
public enum BugResolutionEnum {

    代码修复("0"),
    无效bug("1"),
    需求变更("2"),
    设计如此("3"),
    环境问题("4"),
    其他("5"),
    ;
    private String code;

    BugResolutionEnum(String code){
        this.code =code;
    }

    public String getCode() {
        return code;
    }

    public static BugResolutionEnum getByServerity(String code) {
        for (BugResolutionEnum bugResolutionEnum : BugResolutionEnum.values()) {
            if (bugResolutionEnum.getCode().equals(code)) {
                return bugResolutionEnum;
            }

        }
        return null;
    }

}
