package com.lufax.bugSearch.util;

import com.lufax.bugSearch.controller.Bugstat;
import com.lufax.bugSearch.domain.Group;
import com.lufax.bugSearch.service.Impl.BugServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.*;

/**
 * Created by hongbo010 on 2019/12/29.
 */
public class Sorted {

    public static LinkedHashMap<String, Integer> sortedMapInteger(Map<String, Integer> map) {
        //拿到map的键值对集合
        Set<Map.Entry<String, Integer>> set = map.entrySet();

        //将这个set集合转为list集合为了使用工具类的排序方法
        List<Map.Entry<String, Integer>> list = new ArrayList<>(set);

        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
                    @Override
                    public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                        return o2.getValue() - (o1.getValue());
                    }
                }
        );
        LinkedHashMap<String, Integer> linkedHashMap = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> entry : list) {
            linkedHashMap.put(entry.getKey(), entry.getValue());
        }
        return linkedHashMap;
    }

    public static void main(String args[]) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        BugServiceImpl bugServiceImpl = (BugServiceImpl) context.getBean("bugServiceImpl");
        Group jvFristGroup = Group.getJvFristGroup();
        Bugstat bugstat = new Bugstat();
       // HashMap hashMap3 = bugstat.jvBugCount();
        // HashMap hashMap2 = bugstat.jvServerityBugCount();
      //  Map linkedHashMap = Sorted.sortedMapInteger(hashMap3);
      //  System.out.println(linkedHashMap);
    }
}
