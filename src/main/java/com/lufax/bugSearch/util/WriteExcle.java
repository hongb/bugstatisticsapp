package com.lufax.bugSearch.util;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;


/**
 * Created by hongbo010 on 2019/12/26.
 */
public class WriteExcle {
    private static List<String> SHEET_FIRST_CELL_HEADS; //第一个sheet列头
    private static List<String> SHEET_SECOND_CELL_HEADS;
    private static List<String> SHEET_THIRD_CELL_HEADS;
    private static List<String> SHEET_ONE_CELL_HEADS; //第一个sheet列头
    private static List<String> SHEET_TWO_CELL_HEADS;
    private static List<String> SHEET_THREE_CELL_HEADS;
    private static List<String> SHEET_FOUR_CELL_HEADS;
    private static List<String> SHEET_FIVE_CELL_HEADS;
    private static List<String> SHEET_SIX_CELL_HEADS;

    static {
        // 类装载时就载入指定好的列头信息，如有需要，可以考虑做成动态生成的列头
        SHEET_FIRST_CELL_HEADS = new ArrayList<>();
        SHEET_SECOND_CELL_HEADS = new ArrayList<>();
        SHEET_THIRD_CELL_HEADS = new ArrayList<>();
        SHEET_ONE_CELL_HEADS = new ArrayList<>();
        SHEET_TWO_CELL_HEADS = new ArrayList<>();
        SHEET_THREE_CELL_HEADS = new ArrayList<>();
        SHEET_FOUR_CELL_HEADS = new ArrayList<>();
        SHEET_FIVE_CELL_HEADS = new ArrayList<>();
        SHEET_SIX_CELL_HEADS = new ArrayList<>();
        Collections.addAll(SHEET_FIRST_CELL_HEADS, "name", "1023迭代数", "1106迭代数", "1120迭代数", "1204迭代数", "1218迭代数", "0102迭代数", "bug总数");
        Collections.addAll(SHEET_SECOND_CELL_HEADS, "name", "致命bug", "严重bug", "一般bug", "小错误", "bug总数");
        Collections.addAll(SHEET_THIRD_CELL_HEADS, "排名(总迭代）", "name", "bug数量");
        Collections.addAll(SHEET_ONE_CELL_HEADS, "排名(1023迭代）", "name", "bug数量");
        Collections.addAll(SHEET_TWO_CELL_HEADS, "排名(1106迭代）", "name", "bug数量");
        Collections.addAll(SHEET_THREE_CELL_HEADS, "排名(1120迭代）", "name", "bug数量");
        Collections.addAll(SHEET_FOUR_CELL_HEADS, "排名(1204迭代）", "name", "bug数量");
        Collections.addAll(SHEET_FIVE_CELL_HEADS, "排名(1218迭代）", "name", "bug数量");
        Collections.addAll(SHEET_SIX_CELL_HEADS, "排名(0102迭代）", "name", "bug数量");
    }

    public static void writeExcel(String finalXlsxPath, Map<String, List> map1, Map<String, List> map2, Map<String, Integer> map3) throws IOException {
        Workbook workBook = new SXSSFWorkbook();
        //修改sheet名记得要在addhead方法中修改对应的casename
        workBook.createSheet("迭代版本bug总数统计");   //创建第一个sheet表
        workBook.createSheet("1023迭代以来bug严重程度总数的统计");  //创建第二个sheet表
        workBook.createSheet("每个迭代bug排名统计");   //创建第三个sheet表
        Sheet bugCountByIterVersionSheet = workBook.getSheetAt(0);
        Sheet bugSeverityCountSheet = workBook.getSheetAt(1);
        Sheet bugTopFiveBySheet = workBook.getSheetAt(2);

        /**
         * 往Excel中写新数据
         */

        wirteBugCountByIterVersionSheet(bugCountByIterVersionSheet, map1);  //写第一个sheet内容：迭代版本bug总数统计
        wirteBugSeverityCountSheet(bugSeverityCountSheet, map2);   //写第二个sheet内容：bug严重程度总数的统计
        wirteBugRankSheet(bugTopFiveBySheet, map3);

        try {
            OutputStream out = new FileOutputStream(finalXlsxPath);
            workBook.write(out);
            if (out != null) {
                out.flush();
                out.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("数据导出成功");
    }

    /**
     * 添加列头名称
     */
    public static void addHeadName(Sheet sheet) {
        Row head = sheet.createRow(0);
        switch (sheet.getSheetName()) {
            case "迭代版本bug总数统计":
                for (int i = 0; i < SHEET_FIRST_CELL_HEADS.size(); i++) {
                    Cell cell = head.createCell(i);
                    cell.setCellValue(SHEET_FIRST_CELL_HEADS.get(i));
                }
                break;
            case "1023迭代以来bug严重程度总数的统计":
                for (int i = 0; i < SHEET_SECOND_CELL_HEADS.size(); i++) {
                    Cell cell = head.createCell(i);
                    cell.setCellValue(SHEET_SECOND_CELL_HEADS.get(i));
                }
                break;
            case "每个迭代bug排名统计":
                for (int i = 0; i < SHEET_THIRD_CELL_HEADS.size(); i++) {
                    Cell cell = head.createCell(i);
                    cell.setCellValue(SHEET_THIRD_CELL_HEADS.get(i));
                }
                break;
            case "1023迭代版本bug排名":
                for (int i = 0; i < SHEET_ONE_CELL_HEADS.size(); i++) {
                    Cell cell = head.createCell(i);
                    cell.setCellValue(SHEET_ONE_CELL_HEADS.get(i));
                }
                break;
            case "1106迭代版本bug排名":
                for (int i = 0; i < SHEET_TWO_CELL_HEADS.size(); i++) {
                    Cell cell = head.createCell(i);
                    cell.setCellValue(SHEET_TWO_CELL_HEADS.get(i));
                }
                break;
            case "1120迭代版本bug排名":
                for (int i = 0; i < SHEET_THREE_CELL_HEADS.size(); i++) {
                    Cell cell = head.createCell(i);
                    cell.setCellValue(SHEET_THREE_CELL_HEADS.get(i));
                }
                break;
            case "1204迭代版本bug排名":
                for (int i = 0; i < SHEET_FOUR_CELL_HEADS.size(); i++) {
                    Cell cell = head.createCell(i);
                    cell.setCellValue(SHEET_FOUR_CELL_HEADS.get(i));
                }
                break;
            case "1218迭代版本bug排名":
                for (int i = 0; i < SHEET_FIVE_CELL_HEADS.size(); i++) {
                    Cell cell = head.createCell(i);
                    cell.setCellValue(SHEET_FIVE_CELL_HEADS.get(i));
                }
                break;
            case "0102迭代版本bug排名":
                for (int i = 0; i < SHEET_SIX_CELL_HEADS.size(); i++) {
                    Cell cell = head.createCell(i);
                    cell.setCellValue(SHEET_SIX_CELL_HEADS.get(i));
                }
                break;
        }
    }

    public static void wirteBugCountByIterVersionSheet(Sheet sheet, Map<String, List> map) {
        wirteMapStrAndList(sheet, map);
    }

    public static void wirteBugSeverityCountSheet(Sheet sheet, Map<String, List> map) {
        wirteMapStrAndList(sheet, map);
    }

    public static void wirteBugRankSheet(Sheet sheet, Map<String, Integer> map) {
        wirteMapStrAndInt(sheet, map);
    }

    /**
     * 写map类型通用模式
     *
     * @param1 创建的sheet名称,@param2 map<list>类型数据
     */
    public static void wirteMapStrAndList(Sheet sheet, Map<String, List> map) {
        /*删除原有数据，除属性列
         */
        int rowNumber = sheet.getLastRowNum();    // 第一行从0开始算
        System.out.println("原始数据总行数，除属性列：" + rowNumber);
        for (int i = 1; i <= rowNumber; i++) {
            Row row = sheet.getRow(i);
            sheet.removeRow(row);
        }
        addHeadName(sheet);//添加列头
        rowNumber = 1;
        for (Map.Entry<String, List> entry : map.entrySet()) {
            Row row = sheet.createRow(rowNumber);
            //  System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
            String name = entry.getKey();
            List<Integer> value = entry.getValue();
            Cell first = row.createCell(0);
            first.setCellValue(name);
            for (int i = 1; i <= value.size(); i++) {
                Cell cell = row.createCell(i);
                cell.setCellValue(value.get(i - 1));
            }
            rowNumber++;
        }
    }

    /**
     * 写map类型通用模式
     *
     * @param1 创建的sheet名称,@param2 map<Integer>类型数据
     */
    public static void wirteMapStrAndInt(Sheet sheet, Map<String, Integer> map) {
        addHeadName(sheet);
        int rowNumber = 1;
        LinkedHashMap<String, Integer> linkedHashMap = Sorted.sortedMapInteger(map);
        for (Map.Entry<String, Integer> entry : linkedHashMap.entrySet()) {
            Row row = sheet.createRow(rowNumber);
            Cell first = row.createCell(0);
            first.setCellValue(rowNumber);
            String name = entry.getKey();
            int value = entry.getValue();
            Cell second = row.createCell(1);
            second.setCellValue(name);
            Cell third = row.createCell(2);
            third.setCellValue(value);
            rowNumber++;
        }
    }

}
