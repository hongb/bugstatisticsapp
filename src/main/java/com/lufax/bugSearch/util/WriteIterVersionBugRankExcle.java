package com.lufax.bugSearch.util;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

/**
 * Created by hongbo010 on 2019/12/30.
 */
public class WriteIterVersionBugRankExcle {


    public static void writeExcel(String finalXlsxPath, Map<String, Integer> map1, Map<String, Integer> map2, Map<String, Integer> map3,
                                  Map<String, Integer> map4, Map<String, Integer> map5, Map<String, Integer> map6) throws IOException {
        Workbook workBook = new SXSSFWorkbook();
        //修改sheet名记得要在addhead方法中修改对应的casename
        workBook.createSheet("1023迭代版本bug排名");   //创建第一个sheet表
        workBook.createSheet("1106迭代版本bug排名");  //创建第二个sheet表
        workBook.createSheet("1120迭代版本bug排名");   //创建第三个sheet表
        workBook.createSheet("1204迭代版本bug排名");   //创建第四个sheet表
        workBook.createSheet("1218迭代版本bug排名");  //创建第五个sheet表
        workBook.createSheet("0102迭代版本bug排名");     //创建第六个sheet表
        Sheet oneVersion = workBook.getSheetAt(0);
        Sheet twoVersion = workBook.getSheetAt(1);
        Sheet threeVersion = workBook.getSheetAt(2);
        Sheet fourVersion = workBook.getSheetAt(3);
        Sheet fiveVersion = workBook.getSheetAt(4);
        Sheet sixVersion = workBook.getSheetAt(5);

        /**
         * 往Excel中写新数据
         */
        WriteExcle.wirteMapStrAndInt(oneVersion, map1);
        WriteExcle.wirteMapStrAndInt(twoVersion, map2);
        WriteExcle.wirteMapStrAndInt(threeVersion, map3);
        WriteExcle.wirteMapStrAndInt(fourVersion, map4);
        WriteExcle.wirteMapStrAndInt(fiveVersion, map5);
        WriteExcle.wirteMapStrAndInt(sixVersion, map6);

        try {
            OutputStream out = new FileOutputStream(finalXlsxPath);
            workBook.write(out);
            if (out != null) {
                out.flush();
                out.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("数据导出成功");
    }

}
