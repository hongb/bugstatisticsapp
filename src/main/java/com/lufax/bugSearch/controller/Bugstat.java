package com.lufax.bugSearch.controller;

import com.lufax.bugSearch.domain.Group;
import com.lufax.bugSearch.service.Impl.BugServiceImpl;
import com.lufax.bugSearch.util.WriteExcle;
import com.lufax.bugSearch.util.WriteIterVersionBugRankExcle;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by hongbo010 on 2019/12/26.
 */
public class Bugstat {

    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    BugServiceImpl bugServiceImpl = (BugServiceImpl) context.getBean("bugServiceImpl");
    Group jvFristGroup = Group.getJvFristGroup();


    public static void main(String args[]) throws IOException {

        Bugstat bugstat = new Bugstat();
        // HashMap hashMap = bugstat.jvServerityBugCountByVersion();
        HashMap hashMap1 = bugstat.jvBugCountByVersion();
        HashMap hashMap2 = bugstat.jvServerityBugCount();
        HashMap hashMap3 = bugstat.jvBugSumCount();
        HashMap bugrank1 = bugstat.oneVersionIterBugCount();
        HashMap bugrank2 = bugstat.twoVersionIterBugCount();
        HashMap bugrank3 = bugstat.threeVersionIterBugCount();
        HashMap bugrank4 = bugstat.fourVersionIterBugCount();
        HashMap bugrank5 = bugstat.fiveVersionIterBugCount();
        HashMap bugrank6 = bugstat.sixVersionIterBugCount();
       // List list3 = bugstat.JvBugCount();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date(System.currentTimeMillis());
        String time = formatter.format(date).toString();
        WriteExcle.writeExcel("/Users/hongbo010/Documents/bug" + time + ".xlsx", hashMap1, hashMap2, hashMap3);
        WriteIterVersionBugRankExcle.writeExcel("/Users/hongbo010/Documents/bugRank" + time + ".xlsx", bugrank1, bugrank2, bugrank3, bugrank4, bugrank5, bugrank6);
    }


    /**
     * 查询从1023迭代以来，每个迭代的bug数量的变化，
     *
     * @return HashMap <umid,list>，[0,1,2,3,4,5] 代表1106，1120，1204，1218，0102, 0115迭代的bug数量为0，1，2，3，4，5
     */
    public HashMap jvBugCountByVersion() {
        HashMap hashMap = bugServiceImpl.bugCountByVersion(jvFristGroup);
        return hashMap;
    }

    /**
     * 查询所有bug严重程度总数的统计 --2015年至今的bug
     *
     * @return HashMap <umid,list> [0,1,2,3] 代表致命/严重/一般/小错误 bug数量为0，1，2，3
     */
    public HashMap jvServerityBugCountByAllVersion() {
        HashMap hashMap = bugServiceImpl.groupBugSeverityCount(jvFristGroup);
        return hashMap;
    }

    /**
     * 查询从1023迭代以来，所有bug严重程度总数的统计  2019年10月8日至今的bug
     *
     * @return HashMap <umid,list> [0,1,2,3] 代表致命/严重/一般/小错误 bug数量为0，1，2，3
     */
    public HashMap jvServerityBugCount() {
        HashMap hashMap = bugServiceImpl.GroupCountSeverityBugByVersion(jvFristGroup);
        return hashMap;
    }

    /**
     * 查询从1023迭代以来，每个版本所有bug严重程度总数的统计
     *
     * @return HashMap <umid,list> [0,1,2,3] 代表致命/严重/一般/小错误 bug数量为0，1，2，3
     */
    public HashMap jvServerityBugCountByVersion() {
        HashMap severityBugCountByVersionMap = bugServiceImpl.severityBugCountByVersion(jvFristGroup);
        return severityBugCountByVersionMap;
    }

    /**
     * 查询1023迭代以来，开发的bug数量，
     *
     * @return hashMap eg: hongbo001 = 10, hongbo010 = 20
     */
    public HashMap<String, Integer> jvBugSumCount() {
        return bugServiceImpl.bugCount(jvFristGroup);
    }

    /**
     * 1023迭代bug
     */
    public HashMap oneVersionIterBugCount() {
        return bugServiceImpl.oneVersionIterBugCount(jvFristGroup);
    }

    /**
     * 1106迭代bug
     */
    public HashMap twoVersionIterBugCount() {
        return bugServiceImpl.twoVersionIterBugCount(jvFristGroup);
    }

    /**
     * 1120迭代bug
     */
    public HashMap threeVersionIterBugCount() {
        return bugServiceImpl.threeVersionIterBugCount(jvFristGroup);
    }

    /**
     * 1204版本迭代开发bug统计
     *
     * @return int count
     */
    public HashMap fourVersionIterBugCount() {
        return bugServiceImpl.fourVersionIterBugCount(jvFristGroup);
    }

    /**
     * 1218版本迭代开发bug统计
     *
     * @return int count
     */
    public HashMap fiveVersionIterBugCount() {
        return bugServiceImpl.fiveVersionIterBugCount(jvFristGroup);
    }

    /**
     * 0102版本迭代开发bug统计
     *
     * @param
     * @return int count
     */
    public HashMap sixVersionIterBugCount() {
        return bugServiceImpl.sixVersionIterBugCount(jvFristGroup);
    }

    public List<HashMap> JvBugCount() {
        HashMap<String, Integer> bugCount = bugServiceImpl.bugCount(jvFristGroup);
        HashMap<String, Integer> firstVersionIterBugCount = this.oneVersionIterBugCount();
        HashMap<String, Integer> twoVersionIterBugCount = this.twoVersionIterBugCount();
        HashMap<String, Integer> threeVersionIterBugCount = this.threeVersionIterBugCount();
        HashMap<String, Integer> fourVersionIterBugCount = this.fourVersionIterBugCount();
        HashMap<String, Integer> fiveVersionIterBugCount = this.fiveVersionIterBugCount();
        HashMap<String, Integer> sixVersionIterBugCount = this.sixVersionIterBugCount();
        List list = new ArrayList<HashMap<String, Integer>>();
        Collections.addAll(list, bugCount, firstVersionIterBugCount, twoVersionIterBugCount, threeVersionIterBugCount, fourVersionIterBugCount, fiveVersionIterBugCount, sixVersionIterBugCount);
        return list;
    }

}
