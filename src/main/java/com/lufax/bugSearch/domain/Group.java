package com.lufax.bugSearch.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

    /*
    *Created by hongbo010 on 2019/12/16.
    */

public class Group {

    private String groupName;

    private int groupCode;

    private List<User> groupMembers = new ArrayList<User>() {
    };

    private static Group jvFristGroup = new Group(1);

    private static Group jvTestGroup = new Group(3);

    private static ArrayList<String> jvFristUmidList = new ArrayList<>();

    private static ArrayList<String> jvTestUmidList = new ArrayList<>();


    /*
    * 构造函数，传入groupcode
    * */
    public Group(int groupCode) {
        this.groupCode = groupCode;
        groupName = this.setGroupName(groupCode);
    }

    public static Group getJvFristGroup() {
        Collections.addAll(jvFristUmidList, "xieminghua711", "chenchunyong083", "sunxiaobin485", "chenpeng315", "tiexiaobo197", "caotuo932", "chenwenwei645",
                "weinianjie029", "wubangwei110", "renpan710", "fengxiansen783", "moguansheng279", "shangyueyue088", "liuhenghui743", "zhongzhenhua641","EX-QIANHUI001",
                "EX-SITENGFEI001","EX-ZHANGNAN003","EX-XIALIN001","EX-LIYIMING001","EX-OUZHONGGUANG001"
        );
        for (String jvFristUmid : jvFristUmidList) {
            jvFristGroup.addGroupMember(jvFristUmid);
        }
        return jvFristGroup;
    }

    public static Group getJvTestGroup() {
        Collections.addAll(jvTestUmidList, "hongbo010", "huanghongda424", "ex-wangzhenzhu001", "ex-suxiaona001", "ex-lihui021");
        for (String jvTestUmid : jvTestUmidList) {
            jvTestGroup.addGroupMember(jvTestUmid);
        }
        return jvTestGroup;
    }


    public void setGroupMembers(List<User> groupMembers) {
        this.groupMembers = groupMembers;
    }

    public String getGroupName() {
        return groupName;
    }

    public String setGroupName(int groupCode) {
        switch (groupCode) {
            case 1:
                groupName = "深圳jv开发一团队";
                break;
            case 2:
                groupName = "陆国际团队";
                break;
            case 3:
                groupName = "jv-测试团队";
                break;
            case 4:
                groupName = "JV-native和H5团队";
                break;
            case 5:
                groupName = "jv-后端团队";
                break;
            default:
                groupName = "未知团队";
        }
        return groupName;
    }

    public int getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(int groupCode) {
        this.groupCode = groupCode;
    }

    public List<User> getGroupMembers() {
        return groupMembers;
    }

    public boolean isContainGroup(User user) {
        return groupMembers.contains(user);
    }

    public List<User> addGroupMember(String umid) {
        groupMembers.add(new User(umid));
        return groupMembers;
    }

    public List<String> getGroupListUmid() {
        Iterator it = groupMembers.iterator();
        List<String> umList = new ArrayList<String>() {
        };
        while (it.hasNext()) {
            User user = (User) it.next();
            umList.add(user.getUmid());
        }
        return umList;
    }

    public String getGroupUmid() {
        List umList = this.getGroupListUmid();
        return listToStr(umList);
    }

    public String listToStr(List list) {
        String s = list.toString();

        s = s.replace("[", "('").replace("]", "')").replace(",", "','").replace(" ", "");
        return s;
    }

    @Override
    public String toString() {
        return "Group{" +
                "groupName='" + groupName + '\'' +
                ", groupCode=" + groupCode +
                ", groupMembers=" + groupMembers +
                '}';
    }
}
