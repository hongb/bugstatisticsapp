package com.lufax.bugSearch.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.Calendar;

/**
 * Created by hongbo010 on 2019/12/16.
 */

public class IterTime {
    /*
        * 迭代版本 1023,1106,1120,1204,1211,1218
        * */
    private int iterVersion;


    /*
    * 迭代时间
    * */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8")
    private String iterDate;

    /*
    * 迭代开始时间
    * */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",
                timezone = "GMT+8")
    private Date startIterTime;

    /*
    * 迭代结束时间
    * */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8")
    private Date endIterTime;


    public int getIterVersion() {
        return iterVersion;
    }

    public void setIterVersion(int iterVersion) {
        this.iterVersion = iterVersion;
    }

    public String getIterDate() {
        return iterDate;
    }

    public String setIterTime(Date startIterTime, Date endIterTime) {
        if (startIterTime.compareTo(endIterTime) < 0){
            return null;
        }
        String time = (startIterTime + "-" + endIterTime);
        return time;
    }


    public Date getStartIterTime() {
        return startIterTime;
    }

    public void setStartIterTime(Date startIterTime) {
        this.startIterTime = startIterTime;
    }

    public Date getEndIterTime() {
        return endIterTime;
    }

    public void setEndIterTime(Date endIterTime) {
        if (startIterTime.compareTo(endIterTime)<0) return;
        this.endIterTime = endIterTime;
    }

    public static String dateToStr(Date dateDate) {
           SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
           String dateString = formatter.format(dateDate);
           return dateString;
          }

    public static void main(String args[]) throws ParseException {
        IterTime it = new IterTime();


    }
}
