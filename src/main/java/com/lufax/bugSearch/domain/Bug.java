package com.lufax.bugSearch.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by hongbo010 on 2019/12/16.
 */

@Component
@Scope("prototype")
public class Bug {
    /*
    *bug Id
    */
    private int bugId;

    /*
    *bug标题
    */
    private String sunmmary;

    /*
    *测试人员
    */
    private String tester_owner;

    /*
    * 开发人员
    * */
    private String dev_owner;

    /*
    * 严重程度  致命，严重，一般，小错误
    * */
    private String severity;

    /*
    * bug解决方式
    * */
    private String resolution;

    /*
    * bug创建时间
    * */
    private Date created_at;

    /*
    *bug打开次数
    */
    private int reject_count;

    /*
    * bug数量
    * */
    private int bug_count;

    /*
    * 迭代版本
    * */

    private IterTime iterTime;

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getBugId() {
        return bugId;
    }

    public void setBugId(int bugId) {
        this.bugId = bugId;
    }

    public String getSunmmary() {
        return sunmmary;
    }

    public void setSunmmary(String sunmmary) {
        this.sunmmary = sunmmary;
    }

    public String getTester_owner() {
        return tester_owner;
    }

    public void setTester_owner(String tester_owner) {
        this.tester_owner = tester_owner;
    }

    public String getDev_owner() {
        return dev_owner;
    }

    public void setDev_owner(String dev_owner) {
        this.dev_owner = dev_owner;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public int getReject_count() {
        return reject_count;
    }

    public void setReject_count(int reject_count) {
        this.reject_count = reject_count;
    }

    public int getBug_count() {
        return bug_count;
    }

    public void setBug_count(int bug_count) {
        this.bug_count = bug_count;
    }

    @Override
    public String toString() {
        return "Bug{" +
                "bugId=" + bugId +
                ",userName=" + user.getUsername()+
                ", sunmmary='" + sunmmary + '\'' +
                ", tester_owner='" + tester_owner + '\'' +
                ", dev_owner='" + dev_owner + '\'' +
                ", severity='" + severity + '\'' +
                ", resolution='" + resolution + '\'' +
                ", created_at=" + created_at +
                ", reject_count=" + reject_count +
                ", bug_count=" + bug_count +
                '}';
    }

}
