package com.lufax.bugSearch.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created by hongbo010 on 2019/12/16.
 */
@Component
@Scope("prototype")
public class User {

    /*
    * 姓名
    * */
    private String userName;

    /*
    * Umid
    * */
    private String umid;

    /*
    所属组
    **/
    @Autowired
    private Group group;

    /*
    * 对应的bug
    * */
    private Bug bug;

    public User(String umid){
        this.umid = umid;
    }

    public String getUsername() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUmid() {
        return umid;
    }

    public void setUmid(String umid) {
        this.umid = umid;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Bug getBug() {
        return bug;
    }

    public void setBug(Bug bug) {
        this.bug = bug;
    }
}
